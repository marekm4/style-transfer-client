<?php

require_once '../functions.php';

$jobId = add_job(number_cast($_POST['iterations'], 1, 20));
$uploadDir = get_job_dir($jobId);

mkdir($uploadDir);
move_uploaded_file($_FILES['image']['tmp_name'], $uploadDir . DIRECTORY_SEPARATOR . 'image.jpg');
move_uploaded_file($_FILES['style']['tmp_name'], $uploadDir . DIRECTORY_SEPARATOR . 'style.jpg');

prepare_floyd_hub_job($uploadDir);
update_job_status($jobId, PENDING);

http_redirect('index.php');
