<div class="page-header">
    <h3>Job <?php echo $jobId; ?></h3>
</div>
<div class="row">
    <div class="col-md-4">
        <img src="upload/<?php echo $jobId; ?>/image.jpg" alt="Image" class="img-rounded img-responsive">
    </div>
    <div class="col-md-4">
        <img src="upload/<?php echo $jobId; ?>/style.jpg" alt="Style" class="img-rounded img-responsive">
    </div>
    <div class="col-md-4">
        <img src="upload/<?php echo $jobId; ?>/result.gif" alt="Result" class="img-rounded img-responsive">
    </div>
</div>
