<!DOCTYPE html>
<html>
    <head>
        <title>Style Transfer</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <?php require_once __DIR__ . DIRECTORY_SEPARATOR . $view . '.php'; ?>
        </div>
    </body>
</html>
