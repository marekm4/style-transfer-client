<div class="page-header">
    <h3>Create Job</h3>
</div>
<form action="upload.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label>Image</label>
        <input type="file" name="image" required="">
    </div>
    <div class="form-group">
        <label>Style</label>
        <input type="file" name="style" required="">
    </div>
    <div class="form-group">
        <label>Iterations</label>
        <input type="number" name="iterations" required="" value="10" min="1" max="20">
    </div>
    <button type="submit" class="btn btn-default">Create Job</button>
</form>
<div class="page-header">
    <h3>Jobs</h3>
</div>
<ul>
    <?php foreach ($jobs as $job): ?>
        <li><a href="job.php?id=<?php echo $job['id']; ?>"><?php echo $job['id']; ?> <?php echo $job['status']; ?></a></li>
    <?php endforeach; ?>
</ul>
