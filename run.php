<?php

require_once 'functions.php';

$job = get_next_job(RUNNING);

if ($job) {
    echo 'Check status for job ' . $job['id'] . PHP_EOL;
    $status = get_floyd_hub_job_status($job);
    update_job_status($job['id'], $status);
    if ($status == DONE) {
        download_floyd_hub_job($job);
    }
} else {
    $job = get_next_job(PENDING);
    if ($job) {
        echo 'Found new job ' . $job['id'] . PHP_EOL;
        $floydJobId = run_floyd_hub_job($job);
        update_job_status($job['id'], RUNNING);
        update_job_floyd_hub_id($job['id'], $floydJobId);
    } else {
        echo 'No job to run' . PHP_EOL;
    }
}
