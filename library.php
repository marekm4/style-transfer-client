<?php

function envs_load() {
    $filename = __DIR__ . DIRECTORY_SEPARATOR . '.env';
    if (file_exists($filename)) {
        foreach (file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) as $line) {
            $pos = strpos($line, '=');
            $_SERVER[trim(substr($line, 0, $pos))] = trim(substr($line, $pos + 1));
        }
    }
}

function flash_data() {
    if (isset($_SESSION['flash'])) {
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }
}

function csrf_token() {
    if (empty($_SESSION['csrf'])) {
        $_SESSION['csrf'] = md5(rand(0, 1e9) * time());
    }
    return $_SESSION['csrf'];
}

function number_cast($number, $min = 0, $max = 0) {
    $number = intval($number);
    if ($max > $min) {
        $number = min($number, $max);
    }
    return max($number, $min);
}

function text_encode($text, $length = 0) {
    $text = htmlspecialchars(trim($text), ENT_QUOTES);
    if ($length > 0) {
        $text = substr($text, 0, $length);
    }
    return $text;
}

function cache_fetch($key) {
    $path = __DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $key;
    if (file_exists($path)) {
        $cache = unserialize(file_get_contents($path));
        if (!$cache['expire'] || time() < $cache['expire']) {
            return $cache['value'];
        }
    }
}

function cache_store($key, $value, $expire = 0) {
    if ($expire) {
        $expire += time();
    }
    $cache = serialize(['expire' => $expire, 'value' => $value]);
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $key, $cache);
}

function database_handle() {
    static $handle = null;
    if (is_null($handle)) {
        $handle = pg_connect($_SERVER['DATABASE_URL']);
    }
    return $handle;
}

function database_query($query) {
    return pg_query(database_handle(), $query);
}

function database_scalar($query) {
    return pg_fetch_array(database_query($query))[0];
}

function database_row($query) {
    return pg_fetch_assoc(database_query($query));
}

function database_table($query) {
    return pg_fetch_all(database_query($query)) ?: [];
}

function log_append($message) {
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'log', date('c') . PHP_EOL . $message . PHP_EOL, FILE_APPEND);
}

function ajax_call() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
}

function html_template($view, $data = [], $template = 'main') {
    extract($data);
    require_once 'template' . DIRECTORY_SEPARATOR . $template . '.php';
}

function json_response($data) {
    header('Content-Type: application/json');
    echo json_encode($data);
}

function http_redirect($url, $flash = null) {
    $_SESSION['flash'] = $flash;
    header('Location: ' . $url);
    exit;
}
