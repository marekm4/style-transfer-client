<?php

require_once 'library.php';
envs_load();

const UPLOAD_DIR_BASE = __DIR__ . DIRECTORY_SEPARATOR . 'public/upload';
const SCRIPT_NAME = 'neural_style_transfer.py';

const CREATED = 'CREATED';
const PENDING = 'PENDING';
const RUNNING = 'RUNNING';
const FAILED = 'FAILED';
const DONE = 'DONE';

function get_jobs() {
    return database_table('select * from jobs order by id');
}

function add_job($iterations) {
    return database_scalar('insert into jobs (status, iterations) values (\'' . CREATED . '\', ' . $iterations . ') returning id');
}

function update_job_status($id, $status) {
    database_query('update jobs set status=\'' . $status . '\' where id=' . $id);
}

function update_job_floyd_hub_id($id, $floydhubId) {
    database_query('update jobs set floydhub_id=' . $floydhubId . ' where id=' . $id);
}

function get_next_job($status) {
    return database_row('select * from jobs where status=\'' . $status . '\' order by id');
}

function get_job_dir($jobId) {
    return UPLOAD_DIR_BASE . DIRECTORY_SEPARATOR . $jobId;
}

function shell_exec_with_log($command) {
    putenv('LC_ALL=C.UTF-8');
    putenv('LANG=C.UTF-8');
    $out = shell_exec($command);
    log_append($command . PHP_EOL . $out);
    return $out;
}

function prepare_floyd_hub_job($dir) {
    shell_exec_with_log('cd ' . $dir . ' 2>&1 && convert *.jpg -set filename:base "%[base]" "%[filename:base].jpg" 2>&1');

    $floyd = [
        'namespace' => $_SERVER['FLOYDHUB_NAMESPACE'],
        'name' => $_SERVER['FLOYDHUB_NAME'],
        'family_id' => $_SERVER['FLOYDHUB_FAMILY_ID'],
    ];
    file_put_contents($dir . DIRECTORY_SEPARATOR . '.floydexpt', json_encode($floyd));

    copy(__DIR__ . DIRECTORY_SEPARATOR . SCRIPT_NAME, $dir . DIRECTORY_SEPARATOR . SCRIPT_NAME);
}

function get_floyd_hub_job_status($job) {
    $out = shell_exec_with_log('cd ' . get_job_dir($job['id']) . ' 2>&1 && ' . $_SERVER['FLOYDHUB_PATH'] . ' info ' . $job['floydhub_id'] . ' 2>&1');
    if (strstr($out, 'failed')) {
        return FAILED;
    } else if (strstr($out, 'success')) {
        return DONE;
    } else {
        return RUNNING;
    }
}

function run_floyd_hub_job($job) {
    $out = shell_exec_with_log('cd ' . get_job_dir($job['id']) . ' 2>&1 && ' . $_SERVER['FLOYDHUB_PATH'] . ' run --gpu \'python ' . SCRIPT_NAME . ' --iter ' . $job['iterations'] . ' image.jpg style.jpg /output/\' 2>&1');
    return substr(strrchr($out, '/'), 1);
}

function download_floyd_hub_job($job) {
    shell_exec_with_log('cd ' . get_job_dir($job['id']) . ' 2>&1 && ' . $_SERVER['FLOYDHUB_PATH'] . ' data clone ' . $_SERVER['FLOYDHUB_NAMESPACE'] . '/projects/' . $_SERVER['FLOYDHUB_NAME'] . '/' . $job['floydhub_id'] . '/output 2>&1');
    shell_exec_with_log('cd ' . get_job_dir($job['id']) . ' 2>&1 && convert -delay 50 -loop 0 _* result.gif 2>&1');
}
