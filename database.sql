create table jobs
(
    id serial,
    status varchar(255) not null,
    floydhub_id int,
    iterations int not null default 10
);
